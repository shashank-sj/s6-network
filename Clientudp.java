import java.io.*;
import java.net.*;
import java.util.*;

class Clientudp{

    DatagramSocket client = null;
    DatagramPacket recpck = null;
    DatagramPacket sendpck = null;
    String display;

    byte[] recData = new byte[1024];
    byte[] sendData = new byte[1024];

    private Clientudp(int port){
        try{
            client = new DatagramSocket(port);
            Scanner s = new Scanner(System.in);
            InetAddress IPadd = InetAddress.getByName("localhost");
            String sendmsg = s.next();
            sendData = sendmsg.getBytes();
            sendpck = new DatagramPacket(sendData, sendData.length,IPadd,9876);
            client.send(sendpck);
            recpck = new DatagramPacket(recData, recData.length);
            client.receive(recpck);
            display = new String(recpck.getData());
            System.out.println(display);
            client.close();    
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Clientudp newServer = new Clientudp(9876);
    }
}