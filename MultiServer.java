import java.io.*;
import java.net.*;
import java.util.*;

class Multiserver {

    static int i = 0;
    static Vector<ClientHandler> ar = new Vector<>();

    public static void main(String[] args) {
        ServerSocket ss = new ServerSocket(1234);
        Socket s;
        while (true) {
            s = ss.accept();
            System.out.println("New Clinet Request:" + s);
            DataInputStream din = new DataInputStream(s.getInputStream());
            DataOutputStream dout = new DataOutputStream(s.getOutputStream());
            System.out.println("Creating client handler");
            ClientHandler c = new ClientHandler(s, "Client" + i, din, dout);
            Thread t = new Thread(c);
            System.out.println("Adding to active client list");
            ar.add(c);
            t.start();
            i++;
        }
    }
}

class ClientHandler implements Runnable {
    Scanner sc = new Scanner(System.in);
    private String name;
    /* final */ DataInputStream din;
    /* final */ DataOutputStream dout;
    Socket s;
    boolean isOnline;

    public ClientHandler(Socket s, String name, DataInputStream din, DataOutputStream dout) {
        this.din = din;
        this.dout = dout;
        this.s = s;
        this.isOnline = true;
    }

    public void run(){
        String rec;
        while(true){
            try{
                rec=din.readUTF();
                System.out.prinln(rec);
                if(rec.equals("logout")){
                    this.isOnline=false;
                    this.s.close();
                    break;
                }
                Tokenizer st = new Tokenizer(rec,"#");
                String M2s = st.nextToken();
                String recpient=st.nextToken();
                for(ClientHandler mc:chatserver.ar){
                    if(mc.name.equals(recpient)&&mc.isOnline==true){
                        mc.dout.writeUTF(this.name+":"+M2s);
                    }
                }
            }catch(IOException e){
                e.printStackTrace();
            }
        }
        try{
            this.din.close();
            this.dout.close();
        }catch(Exception e){
            e.printStackTrace();
        }    
    }
}