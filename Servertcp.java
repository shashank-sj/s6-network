import java.io.*;
import java.net.*;

class Servertcp{

	private Socket socket = null;
	private ServerSocket server = null;
	private DataInputStream din = null;
	private DataOutputStream dout = null;

	public Servertcp(int port)
	{
		try{
			server = new ServerSocket(port);
			System.out.println("Server Started at Port: "+port);
			Thread.sleep(1000);			
			System.out.println("Waiting.....");
			socket = server.accept();
			System.out.println("Client Request accpeted");
			try{
			din = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
			dout    = new DataOutputStream(socket.getOutputStream()); 
        	}catch(UnknownHostException u) 
        	{ 
            	System.out.println(u); 
        	} catch(IOException i) 
        { 
            System.out.println(i); 
        } 
			String data = "";
			int test = 0;
			while(!data.equals("END"))
			{
				try{
					data = din.readUTF();
					test = Integer.valueOf(data); 			
					System.out.println(test);
					test*=test;
					data = String.valueOf(test);
					Thread.sleep(1000);
					dout.writeUTF(data);
				}
				catch(Exception e){
					e.printStackTrace();
				}
			}
			System.out.println("Closing Connection with Client");
			socket.close();
			din.close();
		}catch(Exception e){
			e.printStackTrace();		
		}
	}
	public static void main(String arg[])
	{
		Servertcp s = new Servertcp(5050);
	}
}