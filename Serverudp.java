import java.io.*;
import java.net.*;
import java.util.*;

class Serverudp{

    DatagramSocket server = null;
    DatagramPacket recpck = null;
    DatagramPacket sendpck = null;
    String display;

    byte[] recData = new byte[1024];
    byte[] sendData = new byte[1024];

    private Serverudp(int port){
        try{
            server = new DatagramSocket(port);
            while(true){
                recpck = new DatagramPacket(recData, recData.length);
                server.receive(recpck);
                display = new String(recpck.getData());
                System.out.println(display);
                InetAddress IPadd = recpck.getAddress();
                int rport = recpck.getPort();
                Scanner s = new Scanner(System.in);
                String sendmsg = s.next();
                sendData = sendmsg.getBytes();
                sendpck = new DatagramPacket(sendData, sendData.length);
                server.send(sendpck);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }
    public static void main(String[] args) {
        Serverudp newServer = new Serverudp(9876);
    }
}