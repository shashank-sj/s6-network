import java.io.*;
import java.nio.*;
import java.nio.MappedByteBuffer;
import java.nio.file.StandardOpenOption;
import java.nio.channels.FileChannel;

public class Shared1 {

    public static void main( String[] args ) throws Throwable {
        File f = new File("test.txt");

        FileChannel channel = FileChannel.open( f.toPath(), StandardOpenOption.READ, StandardOpenOption.WRITE, StandardOpenOption.CREATE );

        MappedByteBuffer b = channel.map( FileChannel.MapMode.READ_WRITE, 0, 2048 );
        CharBuffer charBuf = b.asCharBuffer();

        char[] string = "Hello World\0".toCharArray();
        charBuf.put( string );
        System.out.println( "Waiting for another process......" );
        while( charBuf.get( 0 ) != '\0' )
		Thread.sleep(1000);
        System.out.println( "Finished writing." );
    }
}
